function anual(){
    let MiCanvas2 = document.getElementById("Grafico2").getContext("2d")
        
    var chart = new Chart(MiCanvas2,{
        type:"line",
        data:{
            labels:['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
             'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            datasets:[{
                label:"Visitas",
                lineTension: 0,
                borderColor:"#00BEDB",
                data:[1000,1200,1800,2000,2010,3015,3000,3100,3150,3000,2900,2950],
                
            }]  
        },
        options:{
            scales:{
                yAxes:[{
                        ticks:{
                            beginAtZero:true
                        }
                }],
                xAxes:[{
                       
                        gridLines: {
                        color: "rgba(0, 0, 0, 0)",
                        }
                }]
            }
    }  
    })
}

function mensuales(){
    let MiCanvas2 = document.getElementById("Grafico2").getContext("2d")
        
    var chart = new Chart(MiCanvas2,{
        type:"line",
        data:{
            labels:['1-5', '6-10', '11-15', '16-20', '21-25', '25-30'],
            datasets:[{
                label:"Visitas",
                lineTension: 0,
                borderColor:"#00BEDB",
                data:[350,203,508,390,400,450],
                
            }]  
        },
        options:{
            scales:{
                yAxes:[{
                        ticks:{
                            beginAtZero:true
                        }
                }],
                xAxes:[{
                       
                        gridLines: {
                        color: "rgba(0, 0, 0, 0)",
                        }
                }]
            }
    }  
    })

        
}


function semanales(){
    let MiCanvas2 = document.getElementById("Grafico2").getContext("2d")
        
    var chart = new Chart(MiCanvas2,{
        type:"line",
        data:{
            labels:["Lunes","Martes ","Miércoles","Jueves","Viernes",
            "Sábado","Domingo"],
            datasets:[{
                label:"Visitas",
                lineTension: 0,
                borderColor:"#00BEDB",
                data:[100,59,66,107,150,200,250],
                
            }]  
        },
        options:{
            scales:{
                yAxes:[{
                        ticks:{
                            beginAtZero:true
                        }
                }],
                xAxes:[{
                       
                        gridLines: {
                        color: "rgba(0, 0, 0, 0)",
                        }
                }]
            }
    }  
    })
}

