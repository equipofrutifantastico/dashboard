function Enero(){
    let MiCanvas = document.getElementById("Grafico").getContext("2d")
var chart = new Chart(MiCanvas,{
    type:"bar",
    data:{
        labels:['Galletas','Tequila','Pan','Ron'],
        datasets:[{
            label:['Productos con mayores ingresos'],
            backgroundColor:"#00BEDB",
            borderColor:"rgb(255,255,255)",
            data:[25,39,30,90]
        }]
    },
    options:{
        scales:{
            yAxes:[{
                    ticks:{
                        beginAtZero:true
                    }
            }],
            xAxes:[{
                   
                    gridLines: {
                    color: "rgba(0, 0, 0, 0)",
                    }
            }]
        }
    }
   
})
}
function Febrero(){
    let MiCanvas = document.getElementById("Grafico").getContext("2d")
var chart = new Chart(MiCanvas,{
    type:"bar",
    data:{
        labels:['Galletas','Tequila','Pan','Ron'],
        datasets:[{
            label:['Productos con mayores ingresos'],
            backgroundColor:"#00BEDB",
            borderColor:"rgb(255,255,255)",
            data:[23,67,24,67]
        }]
    },
    options:{
        scales:{
            yAxes:[{
                    ticks:{
                        beginAtZero:true
                    }
            }],
            xAxes:[{
                   
                    gridLines: {
                    color: "rgba(0, 0, 0, 0)",
                    }
            }]
        }
    }
   
})
}
function Marzo(){
    let MiCanvas = document.getElementById("Grafico").getContext("2d")
var chart = new Chart(MiCanvas,{
    type:"bar",
    data:{
        labels:['Galletas','Tequila','Pan','Ron'],
        datasets:[{
            label:['Productos con mayores ingresos'],
            backgroundColor:"#00BEDB",
            borderColor:"rgb(255,255,255)",
            data:[45,78,37,99]
        }]
    },
    options:{
        scales:{
            yAxes:[{
                    ticks:{
                        beginAtZero:true
                    }
            }],
            xAxes:[{
                   
                    gridLines: {
                    color: "rgba(0, 0, 0, 0)",
                    }
            }]
        }
    }
   
})
}
function Abril(){
    let MiCanvas = document.getElementById("Grafico").getContext("2d")
var chart = new Chart(MiCanvas,{
    type:"bar",
    data:{
        labels:['Galletas','Tequila','Pan','Ron'],
        datasets:[{
            label:['Productos con mayores ingresos'],
            backgroundColor:"#00BEDB",
            borderColor:"rgb(255,255,255)",
            data:[46,78,55,51]
        }]
    },
    options:{
        scales:{
            yAxes:[{
                    ticks:{
                        beginAtZero:true
                    }
            }],
            xAxes:[{
                   
                    gridLines: {
                    color: "rgba(0, 0, 0, 0)",
                    }
            }]
        }
    }
   
})
}
function Mayo(){
    let MiCanvas = document.getElementById("Grafico").getContext("2d")
var chart = new Chart(MiCanvas,{
    type:"bar",
    data:{
        labels:['Galletas','Tequila','Pan','Ron'],
        datasets:[{
            label:['Productos con mayores ingresos'],
            backgroundColor:"#00BEDB",
            borderColor:"rgb(255,255,255)",
            data:[32,56,71,44]
        }]
    },
    options:{
        scales:{
            yAxes:[{
                    ticks:{
                        beginAtZero:true
                    }
            }],
            xAxes:[{
                   
                    gridLines: {
                    color: "rgba(0, 0, 0, 0)",
                    }
            }]
        }
    }
   
})
}
function Junio(){
    let MiCanvas = document.getElementById("Grafico").getContext("2d")
var chart = new Chart(MiCanvas,{
    type:"bar",
    data:{
        labels:['Galletas','Tequila','Pan','Ron'],
        datasets:[{
            label:['Productos con mayores ingresos'],
            backgroundColor:"#00BEDB",
            borderColor:"rgb(255,255,255)",
            data:[69,24,34,90]
        }]
    },
    options:{
        scales:{
            yAxes:[{
                    ticks:{
                        beginAtZero:true
                    }
            }],
            xAxes:[{
                   
                    gridLines: {
                    color: "rgba(0, 0, 0, 0)",
                    }
            }]
        }
    }
   
})
}
function Julio(){
    let MiCanvas = document.getElementById("Grafico").getContext("2d")
var chart = new Chart(MiCanvas,{
    type:"bar",
    data:{
        labels:['Galletas','Tequila','Pan','Ron'],
        datasets:[{
            label:['Productos con mayores ingresos'],
            backgroundColor:"#00BEDB",
            borderColor:"rgb(255,255,255)",
            data:[25,34,47,89]
        }]
    },
    options:{
        scales:{
            yAxes:[{
                    ticks:{
                        beginAtZero:true
                    }
            }],
            xAxes:[{
                   
                    gridLines: {
                    color: "rgba(0, 0, 0, 0)",
                    }
            }]
        }
    }
   
})
}
function Agosto(){
    let MiCanvas = document.getElementById("Grafico").getContext("2d")
var chart = new Chart(MiCanvas,{
    type:"bar",
    data:{
        labels:['Galletas','Tequila','Pan','Ron'],
        datasets:[{
            label:['Productos con mayores ingresos'],
            backgroundColor:"#00BEDB",
            borderColor:"rgb(255,255,255)",
            data:[98,45,79,41]
        }]
    },
    options:{
        scales:{
            yAxes:[{
                    ticks:{
                        beginAtZero:true
                    }
            }],
            xAxes:[{
                   
                    gridLines: {
                    color: "rgba(0, 0, 0, 0)",
                    }
            }]
        }
    }
   
})
}
function Septiembre(){
    let MiCanvas = document.getElementById("Grafico").getContext("2d")
var chart = new Chart(MiCanvas,{
    type:"bar",
    data:{
        labels:['Galletas','Tequila','Pan','Ron'],
        datasets:[{
            label:['Productos con mayores ingresos'],
            backgroundColor:"#00BEDB",
            borderColor:"rgb(255,255,255)",
            data:[55,34,70,67]
        }]
    },
    options:{
        scales:{
            yAxes:[{
                    ticks:{
                        beginAtZero:true
                    }
            }],
            xAxes:[{
                   
                    gridLines: {
                    color: "rgba(0, 0, 0, 0)",
                    }
            }]
        }
    }
   
})
}
function Octubre(){
    let MiCanvas = document.getElementById("Grafico").getContext("2d")
var chart = new Chart(MiCanvas,{
    type:"bar",
    data:{
        labels:['Galletas','Tequila','Pan','Ron'],
        datasets:[{
            label:['Productos con mayores ingresos'],
            backgroundColor:"#00BEDB",
            borderColor:"rgb(255,255,255)",
            data:[45,80,60,90]
        }]
    },
    options:{
        scales:{
            yAxes:[{
                    ticks:{
                        beginAtZero:true
                    }
            }],
            xAxes:[{
                   
                    gridLines: {
                    color: "rgba(0, 0, 0, 0)",
                    }
            }]
        }
    }
   
})
}
function Noviembre(){
    let MiCanvas = document.getElementById("Grafico").getContext("2d")
var chart = new Chart(MiCanvas,{
    type:"bar",
    data:{
        labels:['Galletas','Tequila','Pan','Ron'],
        datasets:[{
            label:['Productos con mayores ingresos'],
            backgroundColor:"#00BEDB",
            borderColor:"rgb(255,255,255)",
            data:[25,45,90,90]
        }]
    },
    options:{
        scales:{
            yAxes:[{
                    ticks:{
                        beginAtZero:true
                    }
            }],
            xAxes:[{
                   
                    gridLines: {
                    color: "rgba(0, 0, 0, 0)",
                    }
            }]
        }
    }
   
})
}
function Diciembre(){
    let MiCanvas = document.getElementById("Grafico").getContext("2d")
var chart = new Chart(MiCanvas,{
    type:"bar",
    data:{
        labels:['Galletas','Tequila','Pan','Ron'],
        datasets:[{
            label:['Productos con mayores ingresos'],
            backgroundColor:"#00BEDB",
            borderColor:"rgb(255,255,255)",
            data:[90,70,30,50]
        }]
    },
    options:{
        scales:{
            yAxes:[{
                    ticks:{
                        beginAtZero:true
                    }
            }],
            xAxes:[{
                   
                    gridLines: {
                    color: "rgba(0, 0, 0, 0)",
                    }
            }]
        }
    }
   
})
}


