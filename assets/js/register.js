function __(id){
    return document.getElementById(id);
}

function checkName() {
    var name = __('name');
    if(name.value.length < 3 || !(/^([A-Za-zñáéíóú]+[\s]*)+$/.test(name.value))){
        name.classList.add('red');
    }
    else{
        name.classList.remove('red');
    }
}
__('name').addEventListener("keyup", checkName);

function checkLastName() {
    var lname = __('lastname');
    if(lname.value.length < 3 || !(/^([A-Za-zñáéíóú]+[\s]*)+$/.test(lname.value))){
        lname.classList.add('red');
    }
    else{
        lname.classList.remove('red');
    }
}
__('lastname').addEventListener("keyup", checkLastName);

function checkEmail() {
    var mail = __('email');
    if(!(/^[^@]+@[^@]+\.[a-zA-Z]{2,5}$/.test(mail.value))){
        mail.classList.add('red');
    }
    else{
        mail.classList.remove('red');
    }
}
__('email').addEventListener("keyup", checkEmail);

function checkPassword() {
    var password = __('password');
    if(password.value.length < 8){
        password.classList.add('red');
    }
    else{
        password.classList.remove('red');
    }
}
__('password').addEventListener("keyup", checkPassword);

function checkConfirm() {
    var confirm = __('confirm');
    var password = __('password');
    if(confirm.value != password.value  || confirm.value.length < 8){
        confirm.classList.add('red');
    }
    else{
        confirm.classList.remove('red');
    }
}
__('confirm').addEventListener("keyup", checkConfirm);

function validation(){
    var name = __('name'),
        lname = __('lastname'),
        mail = __('email'),
        password = __('password'),
        confirm = __('confirm');

    if(name.value.length < 3 || !(/^([A-Za-zñáéíóú]+[\s]*)+$/.test(name.value))){
        alert('Debes escribir un nombre válido.');
        return false;
    }
    else if(lname.value.length < 3 || !(/^([A-Za-zñáéíóú]+[\s]*)+$/.test(lname.value))){
        alert('Debes escribir un apellido válido.');
        return false;
    }
    else if(!(/^[^@]+@[^@]+\.[a-zA-Z]{2,5}$/.test(mail.value))){
        alert('Debes escribir un correo electrónico válido.');
        return false;
    }
    else if(password.value.length < 8){
        alert('Debes ingresar una contraseña válida.');
        return false;
    }
    else if(confirm.value.length < 8){
        alert('Debes ingresar una contraseña válida.');
        return false;
    }
    else if(confirm.value != password.value){
        alert('Las contraseñas no coinciden.');
        return false;
    }
    setCookie('Name', name.value, 1);
    setCookie('LastName', lname.value, 1);
    setCookie('Mail', mail.value, 1);
    setCookie('Password', password.value, 1);
    alert('El formulario se envió con éxito.');
    return true;
}
function setCookie(setName, setValue, expiration){
    var date = new Date();
    date.setTime(date.getTime()+expiration*24*60*60*1000);
    var expiration = 'expires='+date.toUTCString();
    document.cookie = setName+'='+setValue+';'+expiration+';path=/';
}
function getCookie(getName){
    var cookieName = getName+'=';
    var array = document.cookie.split(';');
    for(let i=0; i<array.length; i++){
        var cookie = array[i];
        while (cookie.charAt(0)==' '){
            cookie = cookie.substring(1);
        }
        if(cookie.indexOf(getName)==0){
            return cookie.substring(cookieName.length, cookie.length);
        }
    }
    return '';
}

