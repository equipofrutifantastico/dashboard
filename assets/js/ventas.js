function anual(){
    //Creamos la tabla
    var ctx = document.getElementById('myChart').getContext('2d');
    var chart = new Chart(ctx, {
        // Seleccionamos el tipo de tabla que queremos
        type: 'line',

        // Asignamos los datos requeridos
        data: {
            labels: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            datasets: [{
                label: 'Ventas anuales',
                backgroundColor: '#00BEDB',
                borderColor: '#3877ff',
                data: [5001, 3200, 6000, 2455, 3100, 3000, 4211, 2514, 3700, 4566, 6500, 8500, 10200]
            }]
        },


  
    });
}

function mensuales(){
    //Creamos la tabla
    var ctx = document.getElementById('myChart').getContext('2d');
    var chart = new Chart(ctx, {
        // Seleccionamos el tipo de tabla que queremos
        type: 'line',

        // Asignamos los datos requeridos
        data: {
            labels: ['1-5', '6-10', '11-15', '16-20', '21-25', '25-30'],
            datasets: [{
                label: 'Ventas mensuales',
                backgroundColor: ' #00BEDB',
                borderColor: '#3877ff',
                data: [600, 520, 200, 750, 654, 850]
            }]
        },

    });
}


function semanales(){
    //Creamos la tabla
    var ctx = document.getElementById('myChart').getContext('2d');
    var chart = new Chart(ctx, {
        // Seleccionamos el tipo de tabla que queremos
        type: 'line',

        // Asignamos los datos requeridos
        data: {
            labels: ['Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo'],
            datasets: [{
                label: 'Ventas semanales',
                backgroundColor: '#00BEDB',
                borderColor: '#3877ff',
                data: [50, 75, 200, 250, 554, 550, 600]
            }]
        },


    });
}

