function __(id){
    return document.getElementById(id);
}
function setCookie(setName, setValue, expiration){
    var date = new Date();
    date.setTime(date.getTime()+expiration*24*60*60*1000);
    var expiration = 'expires='+date.toUTCString();
    document.cookie = setName+'='+setValue+';'+expiration+';path=/';
}
function getCookie(getName){
    var cookieName = getName+'=';
    var array = document.cookie.split(';');
    for(let i=0; i<array.length; i++){
        var cookie = array[i];
        while (cookie.charAt(0)==' '){
            cookie = cookie.substring(1);
        }
        if(cookie.indexOf(getName)==0){
            return cookie.substring(cookieName.length, cookie.length);
        }
    }
    return '';
}

function checkLogin(){
    var email = __('email').value;
    var password = __('password').value;
    var checkEmail = getCookie('Mail');
    var checkPassword = getCookie('Password');

    if(email.length == 0 || password.length == 0){
        alert('No debes dejar campos vacíos.');
    }
    else if(email != checkEmail || password != checkPassword){
        alert('Este usuario no pertenece a un administrador.');
    }
    else{
        alert('Ha ingresado al sistema correctamente.');
            if (window.confirm('Presiona Aceptar para ingresar, o Cancelar para quedarte aquí.')){
                window.open('main.html', '_blank');
            };
    }
}
__('login').addEventListener('click', checkLogin);
